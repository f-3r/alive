require_relative "world"

class Alive
  attr_reader :speed, :size, :world, :generations

  def initialize(options)
    @speed = options.fetch "speed", 0.1
    @size = options.fetch "size", 40
    @world = World.new size, options.fetch("population", 0.10)
    @generations = 0
  end

  def run(options = {})
    loop do
      world.clear
      world.draw
      print "generation: #{generations}"
      world.evolve!
      @generations += 1
      sleep speed
    end
  end
end
