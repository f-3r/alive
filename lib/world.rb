require_relative "cell"

class World
  attr_reader :cells, :size

  def initialize(size = 20, population = 0.15)
    @size = size
    @cells = Array.new(size ** 2) { Cell.new(rand < population) }

    # put a glider on the center of the world
    #@cells[size * (size / 2) - 1 + size / 2] = Cell.alive
    #@cells[size * (size / 2) - 1 + size / 2 + 1] = Cell.alive
    #@cells[size * (size / 2) - 1 + size / 2 - 1] = Cell.alive
    #@cells[size * ((size / 2) - 1) + size / 2] = Cell.alive
    #@cells[size * ((size / 2) - 2) + size / 2 - 1] = Cell.alive
  end

  def draw
    cells.each_slice(size) do |cells|
      puts cells.map(&:draw).join
    end
  end

  def clear
    print "\033[2J\033[;H"
  end

  def neighbours(index)
    x, y = *to_xy(index)

    up_y    = y - 1 < 0 ? size - 1 : y - 1
    left_x  = x - 1 < 0 ? size - 1 : x - 1
    down_y  = y + 1 == size ? 0 : y + 1
    right_x = x + 1 == size ? 0 : x + 1

    up = from_xy(x, up_y)
    down = from_xy(x, down_y)
    left = from_xy(left_x, y)
    right = from_xy(right_x, y)
    up_left = from_xy(left_x, up_y)
    up_right = from_xy(right_x, up_y)
    down_left = from_xy(left_x, down_y)
    down_right = from_xy(right_x, down_y)

    cells.values_at(up_left,
                    up,
                    up_right,
                    left,
                    right,
                    down_left,
                    down,
                    down_right)
  end

  def to_xy(index)
    [index % size, index / size]
  end

  def from_xy(x,y)
    y * size + x
  end

  def evolve!
    @cells = cells.map.with_index do |cell, i|
      alive_neighbours = neighbours(i).count(&:alive?)

      if cell.alive?
        if alive_neighbours < 2 || alive_neighbours > 3
          cell.die!
        else
          cell.live!
        end
      else
        if alive_neighbours == 3
          cell.live!
        else
          cell.die!
        end
      end
    end
  end
end
