class Cell
  attr_reader :alive

  def self.alive
    new true
  end

  def self.dead
    new false
  end

  def initialize(alive)
    @alive = alive
  end

  def alive?
    alive
  end

  def dead?
    !alive
  end

  def die!
    self.class.new(false)
  end

  def live!
    self.class.new(true)
  end

  def draw
    color = (17..231).to_a.sample
    alive ? "\e[38;5;#{color}m⬣ \e[0m" : "\e[30;30;30m⬣ \e[0m"
  end
end
