# Alive

Conway's game of life implementation for the comand line

## Running

```ruby
git clone https://gitlab.com/f-3r/alive.git
cd alive
bin/alive --size=80 --speed=0.2 --population=0.15
```

## License

The gem is available as open source under the terms of the [GNU General Public License](https://www.gnu.org/licenses/gpl.html)
