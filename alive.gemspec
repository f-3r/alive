Gem::Specification.new do |spec|
  spec.name          = "alive"
  spec.version       = "0.0.1"
  spec.authors       = ["Fernando Martínez"]
  spec.email         = ["F-3r@users.noreply.github.com"]

  spec.summary       = "Conway's Game of Life"
  spec.description   = "Simple object-oriented Game of Life implementation on the command line"
  spec.homepage      = "https://gitlab.com/f-3r/alive"
  spec.license       = "GPL"

  spec.files         = Dir.chdir(File.expand_path('..', __FILE__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test)/}) }
  end
  spec.bindir        = "bin"
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.16"
end
